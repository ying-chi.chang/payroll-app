package com.payroll.servlet;

import com.payroll.businessLogic.TimeCard;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "HomeServlet", urlPatterns = {"home"}, loadOnStartup = 2)
public class HomeServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<TimeCard> timeCards = new ArrayList<TimeCard>();

        TimeCard timeCard = new TimeCard("Jonathan Lin", "2018 1 1 00:00:00", "2018 1 1 18:00:00");
        timeCards.add(timeCard);

        timeCard = new TimeCard("Biil Chen", "2018 1 1 00:00:00", "2018 1 1 18:00:00");
        timeCards.add(timeCard);

        request.setAttribute("timeCards", timeCards);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("home.jsp");
        requestDispatcher.forward(request, response);
    }
}