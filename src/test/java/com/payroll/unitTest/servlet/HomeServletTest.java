package com.payroll.unitTest.servlet;

import com.payroll.servlet.HomeServlet;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class HomeServletTest {
    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private RequestDispatcher requestDispatcher;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void doGet() throws Exception {
        // Arrange
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        // Act
        new HomeServlet().doGet(request, response);

        // Assert
        verify(request).getRequestDispatcher("home.jsp");
        verify(request).setAttribute(eq("timeCards"), anyList());
    }
}
