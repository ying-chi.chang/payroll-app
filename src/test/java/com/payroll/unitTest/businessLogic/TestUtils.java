package com.payroll.unitTest.businessLogic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.payroll.businessLogic.*;

public class TestUtils {

    /**
     * Generate a list of TimeCard for testing.
     * All the time card generated will be in same month, and not on Saturday nor Sunday.
     *
     * @param days     Total number of TimeCards, not more than 20
     * @param otPerDay OT hours per day
     * @return A list of TimeCard
     */
    public static List<TimeCard> generateTimeCard(int days, int otPerDay) {
        if (days > 20) {
            throw new IllegalArgumentException("Parameter days could not more than 20");
        }

        List<TimeCard> timeCards = new ArrayList<TimeCard>();
        int d = 1;
        int count = 0;
        while (d < 31 && count < days) {
            Date checkinDate = TimeUtils.buildDate("2014 3 " + d + " 09:00:00");
            if (!TimeUtils.isWeekend(checkinDate)) {
                Date checkoutDate = TimeUtils.buildDate("2014 3 " + d + " " + (18 + otPerDay) + ":00:00");
                timeCards.add(new TimeCard(checkinDate, checkoutDate));
                count++;
            }

            d++;
        }

        return timeCards;
    }
}
